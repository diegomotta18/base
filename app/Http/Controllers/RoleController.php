<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    //
    public function index(){
        $this->authorize('index', User::class);

        $roles = Role::paginate(20);
        return view('roles.index',compact('roles'));
    }

    public function create(){
        $this->authorize('store', User::class);

        return view('roles.create');
    }

    public function store(Request $request){
        $this->authorize('store', User::class);
        $request->validate([
            'name'  => 'required|unique:roles,name',
        ]);

         Role::create(['name' => $request->name]);

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Rol creado!'
        ];

        return redirect()
            ->route('roles.index')
            ->with($notification);
    }

    public function update(Request $request,Role $role){
        $this->authorize('update', User::class);

        $request->validate([
            'name'  => 'required|unique:roles,name',
        ]);

        $role->name = $request->name;

        $role->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Rol modificado!'
        ];

        return redirect()
            ->route('roles.index')
            ->with($notification);
    }

    public function edit(Role $role){
        $this->authorize('update', User::class);

        return view('roles.edit',compact('role'));
    }
    public function destroy(Role $role){
        $this->authorize('destroy', User::class);

        $role->delete();
        return 'ok';
    }

}
