<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    //

    public function index()
    {
        $this->authorize('index', User::class);

        $permissions = Permission::paginate(20);

        return view('permissions.index', compact('permissions'));
    }

    protected function getRoles()
    {
        $roles = [];

        $tmp = Role::orderBy('name', 'ASC')->get()->pluck('name');

        foreach ($tmp as $k => $v) {
            $roles[$v] = $v;
        }

        return $roles;
    }


    protected function getPermissions()
    {
        $permissions = [];

        $tmp = Permission::orderBy('id', 'ASC')->get()->pluck('name');

        foreach ($tmp as $k => $v) {
            $permissions[$v] = $v;
        }

        return $permissions;
    }


    public function create()
    {
        $this->authorize('store', User::class);

        return view('permissions.create');
    }

    public function edit(Permission $permission)
    {
        $this->authorize('update', User::class);

        return view('permissions.edit', compact('permission'));
    }

    public function store(Request $request)
    {
        $this->authorize('store', User::class);

        $request->validate([
            'name' => 'required|unique:permissions,name',
        ]);
        Permission::create(['name' => $request->input('name')]);

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Permiso modificado!'
        ];

        return redirect()
            ->route('permissions.index')
            ->with($notification);
    }

    public function update(Request $request, Permission $permission)
    {
        $this->authorize('update', User::class);

        $request->validate([
            'name' => 'required|unique:permissions,name',
        ]);

        $permission->name = $request->input('name');
        $permission->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Permiso modificado!'
        ];

        return redirect()
            ->route('permissions.index')
            ->with($notification);
    }

    public function destroy(Permission $permission)
    {
        $this->authorize('destroy', User::class);

        $permission->delete();

        return 'ok';
    }

    public function getPermissionAssignedToRole()
    {
        $this->authorize('index', User::class);

        $roles = Role::paginate(10);

        return view('permissions.assigne.permissions_assigned_to_rol', compact('roles'));
    }

    public function editAssignPermissionToRole(Role $role)
    {
        $this->authorize('update', User::class);

        $roles = $this->getRoles();
        $permissions = $this->getPermissions();
        $selected =  array_values($role->getAllPermissions()->pluck('name')->toArray());
        return view('permissions.assigne.edit', compact('selected','permissions','roles','role'));
    }

    public function assignPermissionToRole(Request $request)
    {
        $this->authorize('store', User::class);

        $permissions = $request->input('permissions');
        $role = Role::whereName($request->input('role'))->first();
        $role->syncPermissions($permissions);

        app()['cache']->forget('spatie.permission.cache');

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Permisos asignados!'
        ];

        return redirect()
            ->route('permissions.permissionsAsignedToRol')
            ->with($notification);
    }

    public function createPermissionAssignedToRole()
    {
        $this->authorize('store', User::class);

        $roles = $this->getRoles();
        $permissions = $this->getPermissions();
        $selected = null;
        return view('permissions.assigne.create', compact('selected','roles', 'permissions'));
    }

    public function updateAssignPermissionToRole(Role $role, Request $request)
    {
        $this->authorize('update', User::class);

        $permissions = $request->input('permissions');
        $role->syncPermissions($permissions);

        app()['cache']->forget('spatie.permission.cache');

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Permisos asignados actualizado!'
        ];

        return redirect()
            ->route('permissions.permissionsAsignedToRol')
            ->with($notification);
    }

}
