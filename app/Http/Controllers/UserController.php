<?php

namespace App\Http\Controllers;

use App\Area;
use App\Box;
use App\Jobs\CreateUserJob;
use App\Jobs\SendUser;
use App\LoginModel;
use App\Mail\UpdatePasswordUserMail;
use App\Mail\UserCreated;
use App\User;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', User::class);

        $users = User::with('roles')->paginate(10);

        return view('users.index', compact('users'));
    }


    public function create()
    {

        $roles = $this->getRoles();


        return view('users.create', compact('roles'));
    }

    protected function getRoles()
    {
        $roles = [];

        $tmp = Role::orderBy('name', 'ASC')->get()->pluck('name');

        foreach ($tmp as $k => $v) {
            $roles[$v] = $v;
        }

        return $roles;
    }

    public function store(Request $request)
    {
        $this->authorize('store', User::class);
        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users,email',
            'roles'  => 'required',
        ]);

        $tmp_pass = str_random(12);

        $user = User::create([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'password' => bcrypt($tmp_pass),
        ]);

//        Box::create([
//            'user_id' => $user->id,
//        ]);

        $user->assignRole($request->get('roles'));


        $job = new CreateUserJob($user, $tmp_pass, 'Ha sido dado de alta a ');
        dispatch($job);

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Usuario creado!'
        ];

        return redirect()
            ->route('users.index')
            ->with($notification);
    }

    public function edit(User $user)
    {
        $this->authorize('update', User::class);

        $roles = $this->getRoles();

        return view('users.edit', compact('user', 'roles'));
    }

    public function update(User $user, Request $request)
    {
        $this->authorize('update', User::class);

        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users,email,'.$user->id,
            'role'  => 'required|exists:roles,name',
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->save();

        $user->syncRoles($request->get('role'));

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Usuario actualizado!'
        ];

//        $job = new SendUser($user, null,'Ha sido modificado tu usuario de acceso a ');
//        dispatch($job);

        return redirect()
            ->route('users.index')
            ->with($notification);
    }

    public function destroy($id)
    {
        $user = User::where('id',$id);
        $this->authorize('destroy', User::class);

        $user->delete();
        
        return 'ok';
    }

    public function updatePassword(User $user,Request $request){
        $this->authorize('update_password', User::class);

        $request->validate([
            'password'  => 'required',
        ]);

        $user->password = bcrypt($request->input('password'));
        $user->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'La contraseña del usuario a sido actualizado!'
        ];

        $job = new UpdatePasswordUserMail($user,$request->input('password'));
        dispatch($job);

        return redirect()
            ->route('users.index')
            ->with($notification);
    }

}
