<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user){
        return $user->can('Listar roles');
    }

    public function store(User $user)
    {
        return $user->can('Crear rol');

    }

    public function update(User $user)
    {
        return $user->can('Modificar rol');
    }

    public function destroy(User $user)
    {
        return $user->can('Eliminar rol');
    }

    public function show(User $user){
        return $user->can('Mostrar rol');

    }
}
