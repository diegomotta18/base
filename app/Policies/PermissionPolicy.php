<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function index(User $user){
        return $user->can('Listar permisos');
    }

    public function store(User $user)
    {
        return $user->can('Crear permiso');

    }

    public function update(User $user)
    {
        return $user->can('Modificar permiso');
    }

    public function destroy(User $user)
    {
        return $user->can('Eliminar permiso');
    }

    public function show(User $user){
        return $user->can('Mostrar permiso');

    }
}
