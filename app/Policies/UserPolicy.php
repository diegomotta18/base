<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user){
        return $user->can('Listar usuarios');
    }

    public function store(User $user)
    {
        return $user->can('Crear usuario');
    }

    public function update(User $user)
    {
        return $user->can('Modificar usuario');
    }

    public function destroy(User $user)
    {
        return $user->can('Eliminar usuario');
    }

    public function show(User $user){
        return $user->can('Mostrar usuario');

    }
}
