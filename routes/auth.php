<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 17/01/2019
 * Time: 10:46
 */


Route::get('permissions/index',[
    'uses' => 'PermissionController@index',
    'as' => 'permissions.index'
]);

Route::get('permissions',[
    'uses' => 'PermissionController@getPermissionAssignedToRole',
    'as' => 'permissions.permissionsAsignedToRol'
]);

Route::get('permissions/createPermissionAssignedToRole',[
    'uses' => 'PermissionController@createPermissionAssignedToRole',
    'as' => 'permissions.create.PermissionAssignedToRole'
]);

Route::post('permissions/assignpermissiontorole',[
    'uses' => 'PermissionController@assignPermissionToRole',
    'as' => 'permissions.create.assignPermissionToRole'
]);

Route::put('permissions/assignpermissiontorole/{role}/update',[
    'uses' => 'PermissionController@updateAssignPermissionToRole',
    'as' => 'permissions.update.assignPermissionToRole'
]);

Route::get('permissions/assignpermissiontorole/{role}/edit',[
    'uses' => 'PermissionController@editAssignPermissionToRole',
    'as' => 'permissions.edit.assignPermissionToRole'
]);



Route::get('permissions/create',[
    'uses' => 'PermissionController@create',
    'as' => 'permissions.create'
]);


Route::get('/permissions/{permission}/edit', [
    'uses' => 'PermissionController@edit',
    'as' => 'permissions.edit'
]);


Route::put('/permissions/{permission}/update', [
    'uses' => 'PermissionController@update',
    'as' => 'permissions.update'
]);

Route::put('/permissions/{role}/assigned/edit', [
    'uses' => 'PermissionController@editAssignedToRol',
    'as' => 'permissions.edit.assigned'
]);


Route::delete('/permissions/{permission}/destroy', [
    'uses' => 'PermissionController@destroy',
    'as' => 'permissions.destroy'
]);

Route::post('/permissions/store', [
    'uses' => 'PermissionController@store',
    'as' => 'permissions.store'
]);

/**
 * Roles
 */


Route::get('roles/index',[
   'uses' => 'RoleController@index',
    'as' => 'roles.index'
]);

Route::get('roles/create',[
    'uses' => 'RoleController@create',
    'as' => 'roles.create'
]);


Route::get('/roles/{role}/edit', [
    'uses' => 'RoleController@edit',
    'as' => 'roles.edit'
]);


Route::put('/roles/{role}/update', [
    'uses' => 'RoleController@update',
    'as' => 'roles.update'
]);

Route::delete('/roles/{role}/destroy', [
    'uses' => 'RoleController@destroy',
    'as' => 'roles.destroy'
]);

Route::post('/roles/store', [
    'uses' => 'RoleController@store',
    'as' => 'roles.store'
]);
/**
 * Usuarios
 */

Route::get('/users/index', [
    'uses' => 'UserController@index',
    'as' => 'users.index'
]);


Route::get('/usersj', [
    'uses' => 'UserController@allUser',
    'as' => 'users.all'
]);

Route::get('/users/create', [
    'uses' => 'UserController@create',
    'as' => 'users.create'
]);

Route::post('/users', [
    'uses' => 'UserController@store',
    'as' => 'users.store'
]);

Route::get('/users/{user}/edit', [
    'uses' => 'UserController@edit',
    'as' => 'users.edit'
]);

Route::put('/users/{user}/update', [
    'uses' => 'UserController@update',
    'as' => 'users.update'
]);

Route::delete('/users/{id}/destroy', [
    'uses' => 'UserController@destroy',
    'as' => 'users.destroy'
])->where('id', '[0-9]+');

