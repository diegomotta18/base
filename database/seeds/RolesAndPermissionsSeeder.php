<?php

use App\Box;
use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'god']);
        $role1 = Role::create(['name' => 'admin']);
        $role2 = Role::create(['name' => 'user']);

        $user = User::create([
            'name' => 'Diego Motta',
            'email' => 'diegomotta18@gmail.com',
            'password' => bcrypt('admin#*'),
        ]);

        $user->assignRole('god');


//        Box::create([
//            'user_id' => $user1->id
//        ]);


    }
}
