@extends('layouts.app')

@section('content')

        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                <h5>
                    Permisos
                </h5>

                <div class="ibox-tools">
                    <div class="btn-group">

                    <a href="{{ route('permissions.create') }}" style="margin-top: -8px;" class="btn btn-primary">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Crear permiso
                        </a>
                        </div>
                </div>
            </div>

            <div class="ibox-content">
                <table id="table-permissions" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($permissions as $permission)
                        <tr>
                            <td>{{ $permission->name }}</td>
                            <td>
                                <div class="btn-group" style="display: flex;text-align: center">

                                    <a href="{{ route('permissions.edit', $permission->id) }}"  data-toggle="Tooltip on left" class="btn btn-warning">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>

                                    <button data-id="{{ $permission->id }}"    data-toggle="Tooltip on left"
                                            class="btn btn-destroy btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    @if(!is_null($permissions))
                        {{ $permissions->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {

        $('#table-permissions').on('click', '.btn-destroy', function () {
            var id = $(this).data('id');
            var url = route('permissions.destroy', {permission: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {permissions: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'El usuario ha sido eliminado',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });
    });
</script>
@endpush