    {!! Field::select('roles', $roles, isset($role) ? $role->name : ''  ,[ is_null($selected) ? '' : 'disabled','id'=>'roles','name'=>'role','class'=>'js-states form-control','empty' => ('-- Seleccionar rol --')]) !!}

{!! Form::checkboxes('permissions', $permissions, $selected) !!}


<button type="submit" class="btn btn-primary waves-effect waves-classic">
    <i class="fa fa-save" aria-hidden="true"></i>
    Guardar
</button>