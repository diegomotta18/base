@extends('layouts.app')

@section('content')

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Asignar permisos
            </h5>


        </div>

        <div class="ibox-content">
            <form method="POST" action="{{ route('permissions.create.assignPermissionToRole') }}">
                @csrf
                @include('permissions.assigne.partials.form')

            </form>
        </div>
    </div>
@endsection
@push('styles')
<link href="{{ asset('inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('inspinia/js/plugins/select2/select2.full.min.js') }}"></script>

<script>
    $(document).ready(function () {
//        $('.dropdown-toggle').dropdown();

        $('#roles').select2();
    });
</script>
@endpush