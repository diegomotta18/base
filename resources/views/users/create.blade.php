@extends('layouts.app')

@section('content')
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Agregar Usuario
                </h5>
                <div class="ibox-tools">
                        <a href="{{route('users.index')}}"
                           class="btn btn-default" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;margin-top: -8px;" >
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('users.store') }}">
                    @csrf

                    @include('users.partials.form')
                </form>
            </div>
        </div>
@endsection

@push('styles')
<link href="{{ asset('inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('inspinia/js/plugins/select2/select2.full.min.js') }}"></script>

<script>
    $(document).ready(function () {
//        $('.dropdown-toggle').dropdown();

        $('#roles').select2();
    });
</script>
@endpush
