{!! Field::text('name', isset($user) ? $user->name : '') !!}

{!! Field::email('email', isset($user) ? $user->email : '') !!}

{!! Field::select('roles', $roles, isset($user) ? $user->roles : '' ,['name'=>'roles[]','multiple','class'=>'js-states form-control','empty' => ('Asignar roles')]) !!}


<button type="submit" class="btn btn-primary waves-effect waves-classic">
    <i class="fa fa-save" aria-hidden="true"></i>
    Guardar
</button>