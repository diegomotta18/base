@extends('layouts.app')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Editar rol
            </h5>
            <div class="ibox-tools">
                <div class="btn-group">

                    <a href="{{ route('roles.index') }}"  style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;margin-top: -8px;" class="btn btn-default">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <form method="POST" action="{{ route('roles.update', $role->id) }}">
                @csrf
                @method('PUT')

                @include('roles.partials.form')
            </form>
        </div>
    </div>
@endsection

@push('styles')
<link href="{{ asset('inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('inspinia/js/plugins/select2/select2.full.min.js') }}"></script>
{{--<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>--}}

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

//        $('#areas').select2();
    });
</script>
@endpush