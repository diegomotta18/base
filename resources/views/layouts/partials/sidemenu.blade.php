<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="dropdown profile-element text-center" style="    margin-top: -35px;">
            {{--<a data-toggle="dropdown" class="dropdown-toggle" href="#">--}}


                {{--<span>--}}
                    {{--<img alt="image" class="img-circle" style="max-width: 40px"--}}
                         {{--src="{{ asset('img/default-user.png') }}"/>--}}
                {{--</span>--}}
            {{--</a>--}}

            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="clear">
                    <span class="block m-t-xs">
                        <strong class="font-bold">@if(auth()->user()){{ auth()->user()->name }}@endif
                            <b class="caret"></b>
                        </strong>
                    </span>
                </span>
            </a>

            {{-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li>
                    <a href="{{ route('user.profile') }}">
                        Mi perfil
                    </a>
                </li>
            </ul> --}}
        </div>

        <div class="logo-element">
            I+
        </div>
    </li>



    <li>
        <a href="{{route('home')}}">
            <i class="fa fa-home"></i>
            <span class="nav-label">Home</span>
        </a>

    </li>
    <li>
        <a><i class="fa fa-th-large"></i> <span class="nav-label">Seguridad</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level" aria-expanded="true">
            <li><a href="{{route('users.index')}}">Usuarios</a></li>
            <li><a href="{{route('roles.index')}}">Roles</a></li>
            <li><a href="{{route('permissions.permissionsAsignedToRol')}}">Permisos</a></li>

        </ul>
    </li>
</ul>