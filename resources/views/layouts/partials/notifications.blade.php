<script>
    @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch (type) {
        case 'info':
            {{--swal("Info", "{{ Session::get('message') }}", "info");--}}
            swal({
                text: "{{ Session::get('message') }}",
                type: 'info',
                showConfirmButton:false

            });
            break;

        case 'warning':
            swal("Warning", "{{ Session::get('message') }}", "warning");
            break;

        case 'success':
            swal({
                timer: 1800,
                text: "{{ Session::get('message') }}",
                type: 'success',
                showConfirmButton:false

            });            break;

        case 'error':
            swal("Oops", "{{ Session::get('message') }}", "error");
            break;
    }
    {{Session::forget('message')}}
    {{Session::forget('alert-type')}}

    @endif

</script>