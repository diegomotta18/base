<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Budget') }}</title>


    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">--}}

    <!-- Styles de Inspinia template -->
    <link href="{{ asset('inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('inspinia/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('inspinia/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('inspinia/css/plugins/sweetalert2/sweetalert.min.css') }}" rel="stylesheet">


    @stack('styles')
<body class="body-small fixed-sidebar pace-done skin-3">

<div id="app">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                @include('layouts.partials.sidemenu')
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i
                                    class="fa fa-bars"></i>
                        </a>
                        {{--<form role="search" class="navbar-form-custom" action="search_results.html">--}}
                        {{--<div class="form-group">--}}
                        {{--<input type="text" placeholder="Search for something..." class="form-control"--}}
                        {{--name="top-search" id="top-search">--}}
                        {{--</div>--}}
                        {{--</form>--}}
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="pull-right">
                            <a class="dropdown-item" role="menuitem" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="icon md-power" aria-hidden="true"></i> <i class="fa fa-sign-out"></i> Salir
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>

                </nav>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper wrapper-content">
                            @yield('content')
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>
<!-- Scripts -->

<script src="{{ mix('js/app.js') }}"></script>
{{--<script src="{{ mix('inspinia/font-awesome/js/all.min.js')}}"></script>--}}

<!-- Mainly scripts -->
<script src="{{ asset('inspinia/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{ asset('inspinia/js/plugins/popper.js/dist/umd/popper.min.js') }}"></script>

<script src="{{ asset('inspinia/js/plugins/tooltip.js/dist/umd/tooltip.min.js') }}"></script>



<script src="{{ asset('inspinia/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('inspinia/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{ asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{ asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{ asset('inspinia/js/inspinia.js')}}"></script>
<script src="{{ asset('inspinia/js/plugins/pace/pace.min.js')}}"></script>

<!-- Flot -->
<script src="{{ asset('inspinia/js/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{ asset('inspinia/js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{ asset('inspinia/js/plugins/flot/jquery.flot.resize.js')}}"></script>

<!-- ChartJS-->
<script src="{{ asset('inspinia/js/plugins/chartJs/Chart.min.js')}}"></script>

<!-- Peity -->
<script src="{{ asset('inspinia/js/plugins/peity/jquery.peity.min.js')}}"></script>

<!-- Peity demo -->
<script src="{{ asset('inspinia/js/demo/peity-demo.js')}}"></script>

{{--<!-- Toastr-->--}}
{{--<script src="{{ asset('inspinia/js/plugins/toastr/toastr.js')}}"></script>--}}
<script src="{{ asset('inspinia/js/plugins/sweetalert2/sweetalert.min.js') }}"></script>
@include('layouts.partials.notifications')

@stack('scripts')

